package app

import (
	"encoding/json"
	"log"
)

//PerformAction Performs an action requested by clients
func (c *Client) PerformAction(message []byte) {
	// Declared an empty interface
	var jsonData interface{}
	// log.Println(string(message[:]))
	// Unmarshal or Decode the JSON to the interface.
	if jsonErr := json.Unmarshal(message, &jsonData); jsonErr != nil {
		log.Println("JSON parsing failed " + string(message))
		log.Println(jsonErr.Error())
	} else {
		request := jsonData.(map[string]interface{})
		if request["action"] == nil {
			log.Println("No action was found in the request, aborting now!")
			return
		}
		switch request["action"] {
		case "register":
			//registers particepents for a match
			go c.RegisterParticipent(request)
			break
		case "setParticipentAsServer":
			//register a participent as server
			go c.hub.matches[c.match.ID].SetParticipentAsServer(c)
			break
		case "unsetParticipentAsServer":
			go c.hub.matches[c.match.ID].UnsetParticipentAsServer(c)
			break
		case "setParticipentAsClient":
			//register a participent as server
			go c.hub.matches[c.match.ID].SetParticipentAsClient(c)
			break
		case "unsetParticipentAsClient":
			go c.hub.matches[c.match.ID].UnsetParticipentAsClient(c)
			break
		case "transportToServer":
			go c.hub.matches[c.match.ID].BroadcastToServer(message)
			break
		case "transportToClients":
			go c.hub.matches[c.match.ID].BroadcastToClients(request["connectionIds"].([]interface{}), message)
			break
		case "transport":
			if c.isServer {
				go c.hub.matches[c.match.ID].BroadcastToClients(request["connectionIds"].([]interface{}), message)
			} else {
				go c.hub.matches[c.match.ID].BroadcastToServer(message)
			}
			break
		default:
			break
		}
	}
}
