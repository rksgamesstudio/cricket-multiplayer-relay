# Cricket Multiplayer Socket
This application is a web socket written in go, and is designed to be a relay
server for multiplayer feature for our cricket game. 

## Running the example
```go run ./main.go```