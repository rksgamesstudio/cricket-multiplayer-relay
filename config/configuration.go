package config

import (
	"log"

	"github.com/spf13/viper"
)

//Configuration Wrapper for all configuration details
type Configuration struct {
	Server   ServerConfiguration
	Database DatabaseConfiguration
}

//GetConfiguration Returns a configuration
func GetConfiguration() *Configuration {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	var configuration Configuration

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	return &configuration
}
