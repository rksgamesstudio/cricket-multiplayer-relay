package config

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//DatabaseConfiguration Database configuration details
type DatabaseConfiguration struct {
	ConnectionURI   string
	CollectionNames Collections
	DatabaseName    string
}

//Collections Names of mongodb collections
type Collections struct {
	Profiles      string
	Matches       string
	Friendships   string
	PlayerDevices string
	UserDevices   string
	MatchRequests string
}

//GetMongoClient Returns a mongo client
func (db *DatabaseConfiguration) GetMongoClient() (clientt *mongo.Client, ctxx *context.Context, cancelFunc context.CancelFunc, e error) {

	client, err := mongo.NewClient(options.Client().ApplyURI(db.ConnectionURI))

	if err != nil {
		return nil, nil, nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		cancel()
		return nil, nil, nil, err
	}

	return client, &ctx, cancel, nil
}
