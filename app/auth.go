// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package app

import (
	"context"
	"encoding/json"
	"log"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//RegisterParticipent Add a client as a server
func (c *Client) RegisterParticipent(request map[string]interface{}) {
	//add a record for client and device association
	client, _, cancel, err := c.hub.Configuration.Database.GetMongoClient()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer cancel()

	database := client.Database(c.hub.Configuration.Database.DatabaseName)

	collection := database.Collection(c.hub.Configuration.Database.CollectionNames.Matches)
	id, err := primitive.ObjectIDFromHex(request["matchID"].(string))
	if err != nil {
		log.Printf(err.Error())
		return
	}

	var match Match
	err = collection.FindOne(context.Background(), bson.M{"_id": id}).Decode(&match)

	if err != nil {
		log.Printf(err.Error())
		return
	}
	// fmt.Println(fmt.Sprintf("%#v", match))
	clientID, err := primitive.ObjectIDFromHex(request["clientID"].(string))
	if err != nil {
		log.Printf(err.Error())
		return
	}

	//assign id to client and register the client on hub
	c.ID = clientID
	c.match = &match
	c.DeviceID = request["deviceID"].(string)
	// if !c.isServer {
	// 	c.hub.matches[match.ID].LastClientID++
	// 	c.ConnectionID = c.hub.matches[match.ID].LastClientID
	// }
	//register client on hub
	c.hub.register <- c
}

//Logout Mark a client as logged out
func (c *Client) Logout(request map[string]interface{}) {
	log.Printf("Logging out a client " + strconv.FormatBool(c.isServer))
	c.hub.unregister <- c
}

//OnRegistrationComplete When a client is disconnected from socket
func (m *MatchDetails) OnRegistrationComplete(c *Client) {
	resp := make(map[string]interface{})
	resp["action"] = "participent-connected"
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Participants {
		client.send <- responseByte
	}
}

//SetParticipentAsServer Register a participent as a server
func (m *MatchDetails) SetParticipentAsServer(c *Client) {
	c.isServer = true
	m.Server = c
	resp := make(map[string]interface{})
	resp["action"] = "server-started"
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Participants {
		client.send <- responseByte
	}
}

//SetParticipentAsClient Register a participent as a client
func (m *MatchDetails) SetParticipentAsClient(c *Client) {
	c.isServer = false
	m.LastClientID++
	c.ConnectionID = m.LastClientID
	m.Clients[c.ConnectionID] = c
	resp := make(map[string]interface{})
	resp["action"] = "client-connected"
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	resp["connectionID"] = c.ConnectionID
	resp["address"] = c.conn.UnderlyingConn().LocalAddr().String()
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Participants {
		client.send <- responseByte
	}
	// m.OnClientConnect(c)
}

//UnsetParticipentAsServer Register a participent as a server
func (m *MatchDetails) UnsetParticipentAsServer(c *Client) {
	c.isServer = false
	m.Server = nil
	resp := make(map[string]interface{})
	resp["action"] = "server-stopped"
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Participants {
		client.send <- responseByte
	}
	// m.OnServerDisconnect(c)
}

//UnsetParticipentAsClient Register a participent as a client
func (m *MatchDetails) UnsetParticipentAsClient(c *Client) {
	delete(m.Clients, c.ConnectionID)
	resp := make(map[string]interface{})
	resp["action"] = "client-stopped"
	resp["connectionID"] = c.ConnectionID
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Participants {
		client.send <- responseByte
	}
}

//RemoveParticipent removing participent
func (m *MatchDetails) RemoveParticipent(c *Client) {
	//check current participent role and excute appropritae method
	delete(m.Participants, c.DeviceID)
	if _, ok := m.ParticipantDevices[c.ID]; ok {
		for i, deviceID := range m.ParticipantDevices[c.ID] {
			if deviceID == c.DeviceID {
				m.ParticipantDevices[c.ID] = append(m.ParticipantDevices[c.ID][:i], m.ParticipantDevices[c.ID][i+1:]...)
				break
			}
		}

		if len(m.ParticipantDevices[c.ID]) == 0 {
			delete(m.ParticipantDevices, c.ID)
		}
	}
	resp := make(map[string]interface{})
	resp["action"] = "participent-removed"
	resp["user"] = c.ID.Hex()
	resp["deviceID"] = c.DeviceID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	c.send <- responseByte
	for _, client := range m.Participants {
		client.send <- responseByte
	}
}
