// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package app

import (
	"encoding/json"
	"log"
)

//OnClientDisconnect When a client is disconnected from socket
func (m *MatchDetails) OnClientDisconnect(clientID int) {
	resp := make(map[string]interface{})
	resp["action"] = "client-disconnected"
	resp["ID"] = clientID
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	m.Server.send <- responseByte
}

// //OnClientConnect When a client is connected to socket
// func (m *MatchDetails) OnClientConnect(client *Client) {
// 	resp := make(map[string]interface{})
// 	resp["action"] = "client-connected"
// 	resp["connectionID"] = client.ConnectionID
// 	resp["address"] = client.conn.UnderlyingConn().LocalAddr().String()
// 	responseByte, err := json.Marshal(resp)
// 	if err != nil {
// 		log.Printf(err.Error())
// 		return
// 	}
// 	m.Server.send <- responseByte
// }

//OnServerDisconnect When a server is disconnected
func (m *MatchDetails) OnServerDisconnect(server *Client) {
	resp := make(map[string]interface{})
	resp["action"] = "server-disconnected"
	responseByte, err := json.Marshal(resp)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	for _, client := range m.Clients {
		client.send <- responseByte
	}
}

//BroadcastToClients Send data to clients
func (m *MatchDetails) BroadcastToClients(clientIDs []interface{}, payload []byte) {
	for _, ID := range clientIDs {
		if _, ok := m.Clients[int(ID.(float64))]; ok {
			m.Clients[int(ID.(float64))].send <- payload
		}
	}
}

//BroadcastToServer send data to server
func (m *MatchDetails) BroadcastToServer(payload []byte) {
	//at times client is removed but m.Server reference is not removed
	if m.Server != nil && m.Server.isLoggedIn {
		m.Server.send <- payload
	}
}
