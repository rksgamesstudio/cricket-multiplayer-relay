package app

import "go.mongodb.org/mongo-driver/bson/primitive"

//Profile Details of a player
type Profile struct {
	ID     primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name   string             `bson:"name" json:"name"`
	Level  int                `bson:"level" json:"level"`
	Points int                `bson:"points" json:"points"`
}

//Friendship Friendship relation of a player
type Friendship struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	To        primitive.ObjectID `bson:"to" json:"to"`
	From      primitive.ObjectID `bson:"from" json:"from"`
	Confirmed bool               `bson:"confirmed" json:"confirmed"`
	Pending   bool               `bson:"pending" json:"pending"`
}

//UserDevice Details of a player
type UserDevice struct {
	ID       primitive.ObjectID  `bson:"_id,omitempty" json:"id,omitempty"`
	User     primitive.ObjectID  `bson:"user" json:"user"`
	DeviceID string              `bson:"deviceId" json:"deviceId"`
	Time     primitive.Timestamp `bson:"time" json:"time"`
	Online   bool                `bson:"online" json:"online"`
}

//MatchRequest Details of a match request
type MatchRequest struct {
	ID       primitive.ObjectID  `bson:"_id,omitempty" json:"id,omitempty"`
	User     primitive.ObjectID  `bson:"user" json:"user"`
	DeviceID string              `bson:"deviceId" json:"deviceId"`
	Stadium  string              `bson:"stadium" json:"stadium"`
	Time     primitive.Timestamp `bson:"time" json:"time"`
	IsActive bool                `bson:"isActive" json:"isActive"`
}

//Match Match Details
type Match struct {
	ID                primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	HomeDeviceID      string             `bson:"homeDeviceId" json:"homeDeviceId"`
	HomePlayerProfile Profile            `bson:"homePlayerProfile" json:"homePlayerProfile"`
	AwayDeviceID      string             `bson:"awayDeviceId" json:"awayDeviceId"`
	AwayPlayerProfile Profile            `bson:"awayPlayerProfile" json:"awayPlayerProfile"`
	Stadium           int                `bson:"stadium" json:"stadium"`
	HomeTeam          string             `bson:"homeTeam" json:"homeTeam"`
	HomeTeamPlayers   string             `bson:"homeTeamPlayers" json:"homeTeamPlayers"`
	AwayTeam          string             `bson:"awayTeam" json:"awayTeam"`
	AwayTeamPlayers   string             `bson:"awayTeamPlayers" json:"awayTeamPlayers"`
}

//MatchDetails Structure to hold information about players involved in a match
type MatchDetails struct {
	Match              Match
	Server             *Client
	Clients            map[int]*Client
	LastClientID       int
	Participants       map[string]*Client
	ParticipantDevices map[primitive.ObjectID][]string
}
