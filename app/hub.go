// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package app

import (
	"log"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"heliozen.com/cricket-multiplayer-relay/config"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[primitive.ObjectID]Client

	//Registered matches
	matches map[primitive.ObjectID]*MatchDetails

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.

	unregister chan *Client

	// Server Config
	Configuration config.Configuration
}

//NewHub Create and return an instance of hub
func NewHub(config config.Configuration) *Hub {
	return &Hub{
		broadcast:     make(chan []byte),
		register:      make(chan *Client),
		unregister:    make(chan *Client),
		clients:       make(map[primitive.ObjectID]Client),
		Configuration: config,
		matches:       make(map[primitive.ObjectID]*MatchDetails),
	}
}

//Run bootstrap hub
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.ID] = *client
			if _, ok := h.matches[client.match.ID]; !ok {
				h.matches[client.match.ID] = &MatchDetails{
					Match:              *client.match,
					Clients:            make(map[int]*Client),
					LastClientID:       0,
					Participants:       make(map[string]*Client),
					ParticipantDevices: make(map[primitive.ObjectID][]string),
				}
			}
			h.matches[client.match.ID].Participants[client.DeviceID] = client
			if _, ok := h.matches[client.match.ID].ParticipantDevices[client.ID]; !ok {
				log.Printf("declare new device lists")
				h.matches[client.match.ID].ParticipantDevices[client.ID] = []string{}
			}
			h.matches[client.match.ID].ParticipantDevices[client.ID] = append(h.matches[client.match.ID].ParticipantDevices[client.ID], client.DeviceID)
			client.isLoggedIn = true
			h.matches[client.match.ID].OnRegistrationComplete(client)
		case client := <-h.unregister:
			if client.isLoggedIn {
				if _, ok := h.matches[client.match.ID]; ok {
					h.matches[client.match.ID].RemoveParticipent(client)
					// if client.isServer {
					// 	h.matches[client.match.ID].OnServerDisconnect(*client)
					// 	for _, client := range h.matches[client.match.ID].Clients {
					// 		if _, ok := h.clients[client.ID]; ok {
					// 			delete(h.clients, client.ID)
					// 			close(client.send)
					// 		}
					// 	}
					// 	delete(h.matches, client.match.ID)
					// } else {
					// 	//maybe notify the server
					// 	delete(h.matches[client.match.ID].Clients, client.ConnectionID)
					// 	h.matches[client.match.ID].OnClientDisconnect(client.ConnectionID)
					// }
				}
				client.isLoggedIn = false
			}
			if _, ok := h.clients[client.ID]; ok {
				delete(h.clients, client.ID)
				close(client.send)
			}
		case message := <-h.broadcast:
			for ID, client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, ID)
				}
			}
		}
	}
}
