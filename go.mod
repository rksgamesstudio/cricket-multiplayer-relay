module heliozen.com/cricket-multiplayer-relay

go 1.14

require (
	go.mongodb.org/mongo-driver v1.3.3
	github.com/gorilla/websocket v1.4.2
	github.com/spf13/viper v1.7.0
)
